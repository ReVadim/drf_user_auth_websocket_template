# Подготовка рабочего окружения

[Назад](../README.md)

1. [Установить Ubuntu](https://help.ubuntu.ru/wiki/ubuntu_install)
2. Обновить `sudo apt update && sudo apt upgrade -y`
3. Настроить себе что-нибудь, или нет (опционально)
4. [Установить питон](./non_project_dev_tools/python3.11.md)
5. [Установить poetry](./non_project_dev_tools/poetry.md)
6. [Установить PyCharm](./non_project_dev_tools/pycharm.md)
7. [Установить Docker Desktop](./non_project_dev_tools/docker-desktop.md)
8. [Настроить git, gitlab](./non_project_dev_tools/git_gitlab_settings_up.md)
