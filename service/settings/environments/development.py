import logging
import socket

from settings.components import config
from settings.components.common import (
    DATABASES,
    INSTALLED_APPS,
    MIDDLEWARE,
)
from settings.components.csp import (
    CSP_CONNECT_SRC,
    CSP_IMG_SRC,
    CSP_SCRIPT_SRC,
)

DEBUG = True

ALLOWED_HOSTS = [
    config('DOMAIN_NAME'),
    'localhost',
    '0.0.0.0',  # noqa: S104
    '127.0.0.1',
    '[::1]',
]

INSTALLED_APPS += (
    'apps.examples',

    'debug_toolbar',
    'nplusone.ext.django',

    'django_migration_linter',

    'django_test_migrations.contrib.django_checks.DatabaseConfiguration',

    'extra_checks',
)

MIGRATION_LINTER_OPTIONS = {
    'exclude_apps': ['axes'],
}

INSTALLED_APPS = ('apps.management', ) + INSTALLED_APPS

MIDDLEWARE = ('nplusone.ext.django.NPlusOneMiddleware',) + MIDDLEWARE + (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'querycount.middleware.QueryCountMiddleware',
)

try:
    INTERNAL_IPS = [
        '{0}.1'.format(ip[:ip.rfind('.')])
        for ip in socket.gethostbyname_ex(socket.gethostname())[2]
    ]
except socket.error:  # pragma: no cover
    INTERNAL_IPS = []
INTERNAL_IPS += ['127.0.0.1', '10.0.2.2']


def _custom_show_toolbar(request) -> bool:
    return DEBUG


DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK':
        'settings.environments.development._custom_show_toolbar',
}

CSP_SCRIPT_SRC += ('ajax.googleapis.com',)
CSP_IMG_SRC += ('data:',)
CSP_CONNECT_SRC += ("'self'",)

NPLUSONE_RAISE = True
NPLUSONE_LOGGER = logging.getLogger('django')
NPLUSONE_LOG_LEVEL = logging.WARN
NPLUSONE_WHITELIST = [
    {'model': 'admin.*'},
]

DTM_IGNORED_MIGRATIONS = frozenset((
    ('axes', '*'),
))

EXTRA_CHECKS = {
    'checks': [
        'no-unique-together',
        'no-index-together',
        'model-admin',
        'field-file-upload-to',
        'field-text-null',
        'field-null',
        {'id': 'field-foreign-key-db-index', 'when': 'indexes'},
        'field-default-null',
        'field-choices-constraint',
    ],
}

DATABASES['default']['CONN_MAX_AGE'] = 0

DJANGO_SUPERUSER_USERNAME: str = 'dev'
DJANGO_SUPERUSER_EMAIL: str = 'dev@dev.dev'
DJANGO_SUPERUSER_PASSWORD: str = 'DevUser1!'  # noqa: S105
