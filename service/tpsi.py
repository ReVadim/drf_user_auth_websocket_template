import os
from pathlib import Path

from celery import Celery

app_path = Path(__file__).parent
app_name = 'service'

os.chdir(app_path)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

app = Celery(app_name, broker_connection_retry_on_startup=True)
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
