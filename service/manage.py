#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from pathlib import Path

OS_CWD: str = os.getcwd()
SERVICE_WD = Path(__file__).resolve().parent


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
    try:
        from django.core.management import (  # noqa: WPS433
            execute_from_command_line,
        )
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and \
            available on your PYTHONPATH environment variable? Did you \
            forget to activate a virtual environment?",
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    os.chdir(SERVICE_WD)
    main()
    os.chdir(OS_CWD)
