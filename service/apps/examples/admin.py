from django.contrib import admin

from apps.examples.models import ExampleAsyncTask, ExampleCached


@admin.register(ExampleCached)
class ExampleCachedAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']


@admin.register(ExampleAsyncTask)
class ExampleAsyncTaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'percentage', 'one_percent_delay_milliseconds']
