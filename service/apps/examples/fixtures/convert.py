import json
from pathlib import Path

with Path('./raw.json').open('rt', encoding='utf-8') as fin:
    json_in = json.load(fin)

json_out = [
    {
        'model': 'examples.ExampleCached',
        'pk': blog_post['id'],
        'fields': {
            'title': blog_post['title'],
            'description': blog_post['description'],
            'category': blog_post['category'],
            'photo_url': blog_post['photo_url'],
            'content_text': blog_post['content_text'],
            'content_html': blog_post['content_html'],
            'created_at': blog_post['created_at'],
            'updated_at': blog_post['updated_at'],
        },
    } for blog_post in json_in['blogs']
]

with Path('./ExampleCached.json').open('wt', encoding='utf-8') as fou:
    json.dump(json_out, fou, indent=2)
