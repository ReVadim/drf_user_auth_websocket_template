from django.urls import path
from django.views.generic import TemplateView

from apps.examples import views

app_name = 'examples'

urlpatterns = [
    path('', TemplateView.as_view(template_name='examples/index.html'), name='index'),
    path('cached/', views.CachedTemplateView.as_view(), name='cached'),
    path('tasks/', TemplateView.as_view(template_name='examples/tasks.html'), name='tasks'),
    path('api/tasks/', views.example_tasks, name='api_task_ids'),
    path('api/tasks/<int:pk>/', views.example_task_detailed, name='api_task'),
    path('api/tasks/create/', views.example_task_create, name='api_create_task'),
    path('api/tasks/delete/<int:pk>/', views.example_task_delete, name='api_delete_task'),
]
