from datetime import datetime, timedelta

from django.conf import settings
from django.db import models
import uuid
from .models import SlimUser, UserAction, ConfirmationToken


def add_new_user_action(user: SlimUser, description: str, target: models.Model):
    user_action = UserAction(
        user=user,
        description=description,
        target=target,
    )
    user_action.save()


CONFIRMATION_TOKEN_TTL_SECONDS = settings.CONFIRMATION_TOKEN_TTL_SECONDS


def generate_confirmation_token(user: SlimUser) -> str:
    confirmation_token: ConfirmationToken = ConfirmationToken.objects.get_or_create(user=user)
    confirmation_token.expired = datetime.now() + timedelta(seconds=CONFIRMATION_TOKEN_TTL_SECONDS)
    uuid_token = str(uuid.uuid4())
    confirmation_token.uuid = uuid_token
    confirmation_token.save()
    return uuid_token


def is_token_valid(slug: str, token: str) -> bool:
    try:
        user: SlimUser = SlimUser.objects.get(slug=slug)
    except SlimUser.DoesNotExist:
        return False

    if user.token.expired < datetime.now():
        return False

    return user.token.uuid == token
