import json

import requests
from user_agents import parse

response = requests.get(
    url='https://headers.scrapeops.io/v1/browser-headers',
    params={
        'api_key': '3c2d6de1-1105-4531-8685-00aed85db4d4',
        'num_headers': '2'}
)

data = response.json()['result']

# print(json.dumps(data, indent=2))


for item in data:
    user_agent = item['user-agent']
    print(user_agent)
    parsed_user_agent = parse(user_agent)
    print(parsed_user_agent, end='\n\n\n')
