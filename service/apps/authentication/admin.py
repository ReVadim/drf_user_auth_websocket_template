from django.contrib import admin
from .models.slim_user import (
    SlimUser,
    ConfirmationToken,
    VerifiedUserAgent,
)


@admin.register(SlimUser)
class SlimUserAdmin(admin.ModelAdmin):
    """
    Class SlimUserAdmin displays model fields, has a field search
    """
    list_display = ['slug', 'email', 'mobile_phone_number', 'is_staff', 'is_active']
    readonly_fields = ['slug']
    list_filter = ['is_staff', 'is_active']
    search_fields = ['email', 'mobile_phone_number']
    ordering = ['is_active', 'is_staff']
    prepopulated_fields = {'slug': ('email',)}


@admin.register(ConfirmationToken)
class ConfirmationTokenAdmin(admin.ModelAdmin):
    """
    Confirmation of the uniqueness of the user name and universally unique identifiers
    """
    list_display = ['uuid', 'expired', 'user']
    search_fields = ['user', 'expired']
    ordering = ['expired', ]
    readonly_fields = ['uuid', 'expired']


@admin.register(VerifiedUserAgent)
class VerifiedUserAgentAdmin(admin.ModelAdmin):
    """
    User agent confirmation. We check the previously connected device
    """
    list_display = ['user_agent', 'expired', 'user']
    readonly_fields = ['user_agent', 'user']
    ordering = ['expired', ]
